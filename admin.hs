module Admin (addClient, altClient, rmClient, updateClient, 
              addService, altService, rmService, updateService, 
              addOrdemServico, altOrdemServico, updateOrdemServico,
              addItemOrdemServico, updateItemOrdemServico, 
              clienteValido, servicoValido) where

import Control.Monad
import File
import Relatorio
import Numeric
import Data.List

stringify :: Int -> [String] -> String
stringify _ [] = ""
stringify c a = (show c)++","++(conc a)

stringifyItemOrdem :: Int -> Int -> [String] -> String
stringifyItemOrdem _ _ [] = ""
stringifyItemOrdem codOrdem codItem info = (show codOrdem)++","++(show codItem)++","++(conc info)

block :: Int -> [String] -> String
block _ [] = ""
block n x = aux_block 1 n x where
    aux_block _ _ [] = ""
    aux_block i n (a:b) | i < n = a++"\n"++(aux_block (i+1) n b)
                        | otherwise = a

conc [] = ""
conc (a:b) | b == [] = a
           | otherwise = a++","++(conc b)

findCod :: [String] -> Int
findCod [] = 1
findCod x = (read (maximum(map (\y -> (getN (splitOnComma y) 0)) x)) :: Int) + 1

findCodItem :: Int -> Int
findCodItem 0 = 1
findCodItem i = i + 1

-- CLIENTE -----------------------------------------
addClient [] = return ()
addClient x = do
    client_ <- readFile "cliente.db"
    let client_lines_ = splitOnNewLine client_
    let cod = (findCod client_lines_)
    let new_client = client_lines_ ++ [(stringify cod x)]
    let prepare_block = block (length new_client) new_client
    writeFile "cliente_b.db" prepare_block
    return ()

altClient :: String -> [String] -> IO ()
altClient _ [] = return ()
altClient "0" _ = return ()
altClient i x = do
    client_ <- readFile "cliente.db"
    let client_lines_ = splitOnNewLine client_
    let new_client = filter (\y -> (getN (splitOnComma y) 0) /= i) client_lines_ ++ [(stringify (read i :: Int) x)]
    let prepare_block = block (length new_client) new_client
    writeFile "cliente_b.db" prepare_block
    return ()


rmClient [] = return ()
rmClient x = do
    ordem_servico_ <- readFile "ordemservico.db"
    let cod_ordemservico = map (\y -> (getN (splitOnComma y) 0)) (splitOnNewLine ordem_servico_)
    if (elem x cod_ordemservico)
        then return ()
        else do
            client_ <- readFile "cliente.db"
            let client_lines_ = splitOnNewLine client_
            let new_client = filter (\y -> (getN (splitOnComma y) 0) /= x) client_lines_
            let prepare_block = block (length new_client) new_client
            writeFile "cliente_b.db" prepare_block
            return ()

updateClient = do
    new_client <- readFile "cliente_b.db"
    writeFile "cliente.db" new_client
    return ()

-- SERVICO -----------------------------------------
addService [] = return ()
addService x = do
    service_ <- readFile "servico.db"
    let service_lines_ = splitOnNewLine service_
    let cod = (findCod service_lines_)
    let new_service = service_lines_ ++ [(stringify cod x)]
    let prepare_block = block (length new_service) new_service
    writeFile "servico_b.db" prepare_block
    return ()

altService :: String -> [String] -> IO ()
altService _ [] = return ()
altService "0" _= return ()
altService i x = do
    client_ <- readFile "servico.db"
    let client_lines_ = splitOnNewLine client_
    let new_service = filter (\y -> (getN (splitOnComma y) 0) /= i) client_lines_ ++ [(stringify (read i :: Int) x)]
    let prepare_block = block (length new_service) new_service
    writeFile "servico_b.db" prepare_block
    return ()

rmService [] = return ()
rmService x = do
    ordem_servico_ <- readFile "ordemservico.db"
    let cod_ordemservico = map (\y -> (getN (splitOnComma y) 0)) (splitOnNewLine ordem_servico_)
    if (elem x cod_ordemservico)
        then return ()
        else do
            servico_ <- readFile "servico.db"
            let servico_lines_ = splitOnNewLine servico_
            let new_servico = filter (\y -> (getN (splitOnComma y) 0) /= x) servico_lines_
            let prepare_block = block (length new_servico) new_servico
            writeFile "servico_b.db" prepare_block
            return ()

updateService = do
    new_service <- readFile "servico_b.db"
    writeFile "servico.db" new_service
    return ()

-- ORDEM DE SERVICO -----------------------------------------
clienteValido = do
    cod <- getLine
    cliente_ <- readFile "cliente.db"
    let cliente_lines_ = splitOnNewLine cliente_
    if ((busca cliente_lines_ cod) == False)
        then do
            print "erro, cliente invalido, entrar com outro"
            clienteValido
        else
            return cod
            
servicoValido = do
    cod <- getLine
    servico_ <- readFile "servico.db"
    let servico_lines_ = splitOnNewLine servico_
    if ((busca servico_lines_ cod) == False)
        then do
            print "erro, servico invalido, entrar com outro"
            servicoValido
        else
            return cod

busca :: [String] -> String -> Bool
busca [] _ = False
busca (a:b) cod | (cod == (getN (splitOnComma a) 0)) = True
                       | otherwise = busca b cod

addOrdemServico [] = return ()
addOrdemServico x = do
    ordem_servico_ <- readFile "ordemservico.db"
    let ordem_servico_lines_ = splitOnNewLine ordem_servico_
    let cod = (findCod ordem_servico_lines_)
    let value = "0.00"
    let nova_ordem = ordem_servico_lines_ ++ [(stringify cod x)++","++value]
    let prepare_block = block (length nova_ordem) nova_ordem
    writeFile "ordemservico_b.db" prepare_block
    return ()

altOrdemServico = do
    ordem_servico_ <- readFile "ordemservico.db"
    let ordem_servico_lines_ = splitOnNewLine ordem_servico_
    let ordem_a_atualizar_ = splitOnComma (last ordem_servico_lines_)
    let cod = getN ordem_a_atualizar_ 0
    item_ordem_servico_ <- readFile "itemordemservico.db"
    let item_ordem_servico_lines_ = (splitOnNewLine item_ordem_servico_)

    let priceOrdem = sumPriceItemsOrdemServico item_ordem_servico_lines_ cod
    let ordem_servico_atualizada = (init ordem_servico_lines_) ++ [conc ((init ordem_a_atualizar_)++[(show priceOrdem)])]
    let prepare_block = block (length ordem_servico_atualizada) ordem_servico_atualizada
    writeFile "ordemservico_b.db" prepare_block
    return ()


sumPriceItemsOrdemServico :: [String] -> String -> Float
sumPriceItemsOrdemServico [] _ = 0.0
sumPriceItemsOrdemServico (a:b) cod | ((cod) == (getN (splitOnComma a) 0)) = ((read (getN (splitOnComma a) 6) :: Float) + 
                                                                             sumPriceItemsOrdemServico b cod)
                                    | otherwise = sumPriceItemsOrdemServico b cod
            
updateOrdemServico = do
    new_ordem_servico <- readFile "ordemservico_b.db"
    writeFile "ordemservico.db" new_ordem_servico
    return ()

addItemOrdemServico :: Int -> [String] -> IO ()
addItemOrdemServico _ [] = return ()
addItemOrdemServico i x = do
    item_ordem_servico_ <- readFile "itemordemservico.db"
    ordem_servico_ <- readFile "ordemservico.db"
    let ordem_servico_lines_ = splitOnNewLine ordem_servico_
    let codOrdem = (findCod ordem_servico_lines_) - 1

    servico_ <- readFile "servico.db"
    let codServico = (getN x 0)
    let servico_lines_ = splitOnNewLine servico_
    let servico_a_alterar = buscaServico codServico servico_lines_
    let servico_qtt = show ((read (getN servico_a_alterar 2) :: Int) + 1)

    let new_service = filter (\y -> (getN (splitOnComma y) 0) /= codServico) servico_lines_ ++ [(stringify (read codServico :: Int) [(getN servico_a_alterar 1), servico_qtt, (last servico_a_alterar)])]
    let prepare_block = block (length new_service) new_service
    writeFile "servico_b.db" prepare_block
    return ()
    updateService
    
    let discount = (validateDiscount (read (getN x 2) :: Float)) / 100
    let price = getN (splitOnComma (getN (filter (\y -> getN (splitOnComma y) 0 == getN (x) 0) (splitOnNewLine servico_)) 0)) 3
    let total = showGFloat (Just 2) ((read price :: Float) * (read (getN x 3) :: Float) * (1 -(discount :: Float))) ""

    let item_ordem_servico_lines_ = splitOnNewLine item_ordem_servico_
    let codItem = (findCodItem i)
    let novo_item = item_ordem_servico_lines_ ++ [(show codOrdem)++","++
                                                   (show codItem)++","++
                                                   (getN x 0)++","++
                                                   price++","++
                                                   (getN x 2)++","++
                                                   (getN x 3)++","++
                                                   total]

    let prepare_block = block (length novo_item) novo_item
    writeFile "itemordemservico_b.db" prepare_block
    return ()

buscaServico :: String -> [String] -> [String]
buscaServico cod (a:b) | (cod == getN (splitOnComma a) 0) = splitOnComma a
                       | otherwise = buscaServico cod b



validateDiscount :: Float -> Float
validateDiscount d | (d < 0) = 0
                   | (d > 15) = 15
                   | otherwise = d

updateItemOrdemServico = do
    new_item_ordem_servico <- readFile "itemordemservico_b.db"
    writeFile "itemordemservico.db" new_item_ordem_servico
    return ()