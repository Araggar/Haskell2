module File (splitOnNewLine) where

import Data.List.Split

splitOnNewLine :: String -> [String]
splitOnNewLine x = splitOn "\n" x

