module File (splitOnComma, splitOnNewLine, getN) where

import Data.List.Split

splitOnNewLine :: String -> [String]
splitOnNewLine [] = []
splitOnNewLine x = splitOn "\n" x

splitOnComma :: String -> [String]
splitOnComma [] = []
splitOnComma x = splitOn "," x

getN :: [String] -> Int -> String
getN [] _ = ""
getN x n = matchN x n 0 where
    matchN :: [String] -> Int -> Int -> String
    matchN (a:b) n t | n == t = a
                     | otherwise = matchN b n (t+1)



