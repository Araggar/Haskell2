module Main (main) where

import Relatorio
import File
import Admin
import System.Exit
import System.IO.Unsafe

{-- --------------------------------------------------- --}
questaoA = do
    menuQuestaoA
    returnToMenuOrQuit

menuQuestaoA :: IO ()
menuQuestaoA = do
      print "Registro de cliente e servico"
      putStrLn . unlines $ map concatNums choicesQuestaoA 
      choice <- getLine
      case validate choice of
         Just n  -> executeQuestaoA . read $ choice
         Nothing -> putStrLn "Please try again"

      menuQuestaoA
   where concatNums (i, (s, _)) = show i ++ ". " ++ s

choicesQuestaoA :: [(Int, (String, IO ()))]
choicesQuestaoA = zip [1.. ] [
   ("Adiciona novo cliente", adicionaCliente)
 , ("Altera cliente cadastrado", alteraCliente)
 , ("Remove cliente cadastrado", removeCliente)
 , ("Adiciona novo servico", adicionaServico)
 , ("Altera servico cadastrado", alteraServico)
 , ("Remove servico cadastrado", removeServico)
 , ("Voltar ao menu principal", menu)
 , ("QUIT", quit)
 ]

executeQuestaoA :: Int -> IO ()
executeQuestaoA n = doExec $ filter (\(i, _) -> i == n) choicesQuestaoA
   where doExec ((_, (_,f)):_) = f

adicionaCliente = do
    let dados = unsafePerformIO dadosCliente
    addClient dados
    updateClient
    returnToMenuOrQuit

adicionaServico = do
    let dados = unsafePerformIO dadosServico
    addService dados
    updateService
    returnToMenuOrQuit

alteraCliente = do
    print "Id do cliente que deseja alterar"
    id <- getLine
    let dados = unsafePerformIO dadosCliente
    altClient id dados
    updateClient
    returnToMenuOrQuit

alteraServico = do
    print "Id do servico que deseja alterar"
    id <- getLine
    let dados = unsafePerformIO dadosServico
    altService id dados
    updateService
    returnToMenuOrQuit

dadosCliente = do
    print "Entrar com os dados do cliente"
    print "Nome"
    print "Cidade"
    print "Idade"
    print "Sexo M/F"
    nome <- getLine
    cidade <- getLine
    idade <- getLine
    sexo <- getLine
    return [nome, cidade, idade, sexo]

dadosServico = do
    print "Entrar com os dados do servico"
    print "Tipo de servico"
    print "Horas para realizar"
    print "Preco por hora"
    servico <- getLine
    horas <- getLine
    preco <- getLine
    return [servico, horas, preco]

removeCliente = do
    print "Id do cliente que deseja remover"
    id <- getLine
    rmClient id
    updateClient
    returnToMenuOrQuit

removeServico = do
    print "Id do servico que deseja remover"
    id <- getLine
    rmService id
    updateService
    returnToMenuOrQuit
{-- --------------------------------------------------- --}
questaoB = do
    print ("Registro de ordem de serviço")
    adicionaOrdemServico
    returnToMenuOrQuit

adicionaOrdemServico = do
    print "Entrar com os dados da ordem de servico"
    print "Dia DD"
    print "Mes MM"
    print "Ano YYYY"
    print "Codigo do cliente"
    dia <- getLine
    mes <- getLine
    ano <- getLine
    let cod = unsafePerformIO clienteValido
    addOrdemServico [cod, dia, mes, ano]
    updateOrdemServico
    adicionaItemOrdemServico 0
    altOrdemServico
    updateOrdemServico

adicionaItemOrdemServico i = do
    print "Entrar com os dados do item da ordem de servico"
    print "Desconto"
    print "Quantidade"
    print "Codigo servico"
    desconto <- getLine
    quantidade <- getLine
    let codServico = unsafePerformIO servicoValido
    addItemOrdemServico i [codServico, "0.00", desconto, quantidade]
    updateItemOrdemServico
    print "Adicionar mais items? Y/N"
    choice <- getLine
    if ((choice == "y") || (choice == "Y"))
        then
            adicionaItemOrdemServico (i + 1)
        else
            return ()
{-- --------------------------------------------------- --}
questaoC = do
    print ("Relatorio de clientes")
    relatorioClientes
    returnToMenuOrQuit

{-- --------------------------------------------------- --}
questaoD = do
    print ("Relatorio de serviços")
    relatorioServico
    returnToMenuOrQuit

{-- --------------------------------------------------- --}
questaoE = do
    print ("Entre com a data dos relatorios que deseja verificar")
    print ("data inicio: YYYYMMDD")
    inicio <- getLine
    print ("data fim: YYYYMMDD")
    final <- getLine
    relatorioServicoTempo inicio final
    returnToMenuOrQuit

{-- --------------------------------------------------- --}
questaoF = do
    print ("Entre com o numero do cliente no qual deseja imprimir relatorio")
    cliente <- getLine
    relatorioCliente cliente
    returnToMenuOrQuit

{-- --------------------------------------------------- --}
questaoG = do
    print ("Entre com a data na qual deseja verificar os clientes com mais solicitaçoes")
    print ("data inicio: YYYYMMDD")
    inicio <- getLine
    print ("data fim: YYYYMMDD")
    final <- getLine
    relatorioClienteEntre inicio final
    returnToMenuOrQuit

{-- --------------------------------------------------- --}
questaoH = do
    print ("Entre com a data na qual deseja verificar os seviços com mais horas de prestaçao")
    print ("data inicio: YYYYMMDD")
    inicio <- getLine
    print ("data fim: YYYYMMDD")
    final <- getLine
    relatorioServicoEntre inicio final
    returnToMenuOrQuit

{-- --------------------------------------------------- --}
questaoJ = do
    print ("Verificar a coerencia do banco de dados")
    returnToMenuOrQuit

{-- --------------------------------------------------- --}
quit = do
    print ("Programa finalizando...")
    exitSuccess

returnToMenuOrQuit = do
    print ("voltar para o menu principal? Y/N")
    opcao <- getLine
    returnOption opcao
    
returnOption :: String -> IO ()
returnOption opcao | (opcao == "y" || opcao == "Y") = return ()
                   | otherwise = quit
    

menu :: IO ()
menu = do
      print "Digite a opcao desejada"
      putStrLn . unlines $ map concatNums choices 
      choice <- getLine
      case validate choice of
         Just n  -> execute . read $ choice
         Nothing -> putStrLn "Please try again"

      menu
   where concatNums (i, (s, _)) = show i ++ ". " ++ s

validate :: String -> Maybe Int
validate s = isValid (reads s)
   where isValid []            = Nothing
         isValid ((n, _):_) 
               | outOfBounds n = Nothing
               | otherwise     = Just n
         outOfBounds n = (n < 1) || (n > length choices)

choices :: [(Int, (String, IO ()))]
choices = zip [1.. ] [
   ("A Registro de cliente ou serviços", questaoA)
 , ("B Registro de ordem de serviço", questaoB)
 , ("C Relatorio de clientes", questaoC)
 , ("D Relatorio de serviços", questaoD)
 , ("E Relatorio de serviços entre datas especificas", questaoE)
 , ("F Relatorio de cliente especifico", questaoF)
 , ("G Relatorio de clientes entre datas especificas", questaoG)
 , ("H Relatorio de serviços com mais horas de prestaçao", questaoH)
 , ("J Verificar coerencia do banco de dados", questaoJ)
 , ("QUIT", quit)
 ]

execute :: Int -> IO ()
execute n = doExec $ filter (\(i, _) -> i == n) choices
   where doExec ((_, (_,f)):_) = f

main = do
    menu