# Haskell2
Trabalho Haskell 2 - Paradigmas

Compilar:
```
ghc -o file main.hs readfile.hs relatorio.hs admin.hs
```

As questoes foram respondidas em forma de um menu, sendo este, de uso intuitivo.

O programa desenvolvido tem as seguites capacidades:
### Cadastro
- Cadastro de usuários
- Cadastro de serviços
- Cadastro de ordem de serviços e seus respectivos items

### Relatorio
- Relatório de todos os clientes
- Relatório de todos os serviços
- Relatório de serviços num determinado período
- Relatório de todas as ordems de serviços para um determinado cliente
- Relatório de clientes com mais solicitações de serviços em determinado período
- Relatório de serviços com mais horas em determinado período