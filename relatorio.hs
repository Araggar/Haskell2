module Relatorio (printItems, getN, relatorioClientes, relatorioServico, relatorioCliente, relatorioServicoTempo, relatorioClienteEntre, relatorioServicoEntre, coerenciaDatabase) where

import File
import Data.List
import Data.Maybe

{-
QUESTAO C
Emissao de relatorio de todos os clientes
-}
relatorioClientes = do
    cliente_ <- readFile "cliente.db"
    let cliente_lines_ = splitOnNewLine cliente_
    printCliente cliente_lines_

{-
QUESTAO D
Emissao de relatorio de serviços
-}
relatorioServico = do
    servico_ <- readFile "servico.db"
    let servico_lines_ = splitOnNewLine servico_
    printItems servico_lines_

{-
QUESTAO E
Emissao de relatorio de ordem de serviços
i: data inicia
f: data final
-}
relatorioServicoTempo i f = do
    servico_ <- readFile "ordemservico.db"
    -- filtra para as ordens de serviço com datas maiores que a data inicial
    let servico_lines_ = (filter (\y -> ((getN (splitOnComma y) 4) ++
                                        (getN (splitOnComma y) 3) ++
                                        (getN (splitOnComma y) 2)) >= i) 
                                 (splitOnNewLine servico_))

    -- filtra para as ordens de serviço com datas menores que data final
    let servico_lines_clean = (filter (\y -> ((getN (splitOnComma y) 4) ++
                                             (getN (splitOnComma y) 3) ++
                                             (getN (splitOnComma y) 2)) <= f) servico_lines_)
    printItems servico_lines_clean

{-
QUESTAO F
Emissao de relatorio de cliente especifico
x: id do cliente
-}
relatorioCliente x = do
    servico_ <- readFile "ordemservico.db"
    -- busca a ordem de serviço com id = x
    let servico_lines_ = (filter (\y -> getN (splitOnComma y) 1 == x) (splitOnNewLine servico_))
    printItems servico_lines_
    print("Total Cliente " ++ x)
    printTotal servico_lines_

{-
QUESTAO G
Emissao relatorio de clientes com mais solicitações em determinado tempo
i: data inical
f: data final
-}
relatorioClienteEntre i f = do
    cliente_ <- readFile "cliente.db"
    let cliente_lines_ = splitOnNewLine cliente_
    servico_ <- readFile "ordemservico.db"

    -- filtra para as ordens de serviço com datas maiores que a data inicial
    let servico_lines_ = (filter (\y -> ((getN (splitOnComma y) 4) ++
                                        (getN (splitOnComma y) 3) ++
                                        (getN (splitOnComma y) 2)) >= i) 
                                 (splitOnNewLine servico_))

    -- filtra para as ordens de serviço com datas menores que data final
    let servico_lines_clean = (filter (\y -> ((getN (splitOnComma y) 4) ++
                                             (getN (splitOnComma y) 3) ++
                                             (getN (splitOnComma y) 2)) <= f) servico_lines_)
    
    -- salva os id's dos clientes das ordems já filtradas
    let cods = (map (\y -> getN (splitOnComma y) 1) (servico_lines_clean))

    -- remove duplicatas
    let cods_clean = set cods [] where
        set [] y = []
        set (a:b) y | elem a y = set b y
                    | otherwise = [a]++(set b ([a]++y))
    let max_cod = maximum cods_clean

    -- monta o array com a resposta
    let final_array = [(a,
                        getNome (read a :: Int) (length cliente_lines_) cliente_lines_, 
                        totalClientEntre (read a :: Int) servico_lines_clean, 
                        qtOrdens (read a :: Int) servico_lines_clean) | a <- cods_clean]
    
    -- ordena da forma explicitada no enunciado
    let final_array_sorted = sortBy mySortFunc final_array

    -- imprime na tela o array com linhas
    printItems final_array_sorted

    -- imprime as datas, inicial e final, alem do total
    print ("Total entre "++i++" e "++f++" = "++(show (totalEntre final_array_sorted))) where
        totalEntre [] = 0
        totalEntre (a:b) = (getCash a) + (totalEntre b) where
            getCash :: (String, String, Float, Int) -> Float
            getCash (a,b,c,d) = c

    {-let names = name_aux max_cod where
        name_aux 0 = []
        name_aux i | elem i cods_clean = [(a,b)|a <- i, b <- (getNome i (length cliente_lines_) cliente_lines_)]++(name_aux (i-1))
                   | otherwise = name_aux (i-1)-}

{-
QUESTAO H
Emissao de relatorio de serviços com maior tempo de prestação de serviço em determinado tempo
i: data inicial
f: data final
-}
relatorioServicoEntre i f = do
    servico_ <- readFile "servico.db"
    let servico_lines_ = splitOnNewLine servico_
    item_ordem_servico_ <- readFile "itemordemservico.db"
    let item_ordem_servico_lines_ = splitOnNewLine item_ordem_servico_
    ordem_servico_ <- readFile "ordemservico.db"

    -- filtra para as ordens de serviço com datas maiores que a data inicial
    let ordem_servico_lines_ = (filter (\y -> ((getN (splitOnComma y) 4) ++
                                               (getN (splitOnComma y) 3) ++
                                               (getN (splitOnComma y) 2)) >= i) 
                                       (splitOnNewLine ordem_servico_))

     -- filtra para as ordens de serviço com datas menores que data final
    let ordem_servico_lines_clean_ = (filter (\y -> ((getN (splitOnComma y) 4) ++
                                                    (getN (splitOnComma y) 3) ++
                                                    (getN (splitOnComma y) 2)) <= f) ordem_servico_lines_)
    
    -- salva o id das ordens de serviço que sao validas
    let id_ordem_servico = (map (\y -> getN (splitOnComma y) 0) (ordem_servico_lines_clean_))

    -- remove duplicatas
    let id_ordem_servico_clean_ = set id_ordem_servico [] where
        set [] y = []
        set (a:b) y | elem a y = set b y
                    | otherwise = [a]++(set b ([a]++y))

    -- salva id dos servicos validos
    let id_servico_valido = getIdServicoValido id_ordem_servico_clean_ item_ordem_servico_lines_

    -- salva a quantidade de horas dos servicos validos
    let quantidade_servico = getQuantidadeServicos id_ordem_servico_clean_ item_ordem_servico_lines_

    -- salva o valor medio desses servicos validos
    let valor_servico = getValorServicos id_ordem_servico_clean_ item_ordem_servico_lines_

    -- os servicos que possuem o mesmo id têm suas horas somadas entre eles
    let quantidade_servico_somado = addEqualElements id_servico_valido quantidade_servico id_servico_valido

    -- os servicos que possuem o mesmo id tem seus valores somados entre eles
    let valor_servico_somado = addValueEqualElements id_servico_valido valor_servico quantidade_servico id_servico_valido

    -- para preservar a ordem o id, quantidade de horas e o valor sao unidos num array de tuplas
    let id_servico_qualidade_preco = unzip3 (sortBy sortLT (nub (zip3 id_servico_valido quantidade_servico_somado valor_servico_somado)))

    -- primeiro array da tupla
    let id_servico_valido_clean = get1th id_servico_qualidade_preco

    -- segundo array da tupla
    let quantidade_horas_clean = get2th id_servico_qualidade_preco

    -- terceiro array da tupla
    let preco_clean = get3th id_servico_qualidade_preco

    -- valor total = quantidade horas * preco
    let valor_total = totalValue quantidade_horas_clean preco_clean

    -- array final montado
    let final_array =   [(a, 
                        getNome (read a :: Int) (length servico_lines_) servico_lines_,
                        getN quantidade_horas_clean (fromJust (elemIndex a id_servico_valido_clean)),
                        getN preco_clean (fromJust (elemIndex a id_servico_valido_clean)),
                        getN valor_total (fromJust (elemIndex a id_servico_valido_clean))) | a <- id_servico_valido_clean]

    let final_array_sorted = sortBy mySortFunc2 final_array
    printItems final_array_sorted 

{-
QUESTAO J
-}
coerenciaDatabase = do
    print ("coerencia")

first :: [t] -> t
first (a:_) = a

rest :: [t] -> [t]
rest (_:b) = b

get1th (a, _, _) = a
get2th (_, a, _) = a
get3th (_, _, a) = a

totalValue _ [] = []
totalValue [] _ = []
totalValue quantidade_horas preco = do
    let x = (read (first quantidade_horas) :: Float) * (read (first preco) :: Float)
    let z = [show x] ++ (totalValue (rest quantidade_horas) (rest preco))
    z

addValueEqualElements _ _ _ [] = []
addValueEqualElements id_servico_valido valor_servico quantidade_servico dummy = do
    let x = elemIndices (first dummy) id_servico_valido
    let w = (sum [read (getN valor_servico a) :: Float | a <- x]) / (sum [read (getN quantidade_servico a) :: Float | a <- x ])
    let z = [show w] ++ (addValueEqualElements (id_servico_valido) valor_servico quantidade_servico (rest dummy))
    z

addEqualElements _ _ [] = []
addEqualElements id_servico_valido quantidade_servico dummy = do
    let x = elemIndices (first dummy) id_servico_valido
    let y = sum [read (getN quantidade_servico a) :: Int | a <- x ]
    let z = [show y] ++ (addEqualElements (id_servico_valido) quantidade_servico (rest dummy))
    z

getIdServicoValido :: [String] -> [String] -> [String]
getIdServicoValido _ [] = []
getIdServicoValido [] _ = []
getIdServicoValido idOrdemServico itensOrdemServico 
    | (first idOrdemServico) == getN (splitOnComma (first itensOrdemServico)) 0 = 
      [(getN (splitOnComma (first itensOrdemServico)) 2)] ++ 
      (getIdServicoValido idOrdemServico (rest itensOrdemServico))
    | otherwise = getIdServicoValido (rest idOrdemServico) itensOrdemServico

getQuantidadeServicos :: [String] -> [String] -> [String]
getQuantidadeServicos _ [] = []
getQuantidadeServicos [] _ = []
getQuantidadeServicos idOrdemServico itensOrdemServico 
    | (first idOrdemServico) == getN (splitOnComma (first itensOrdemServico)) 0 = 
      [(getN (splitOnComma (first itensOrdemServico)) 5)] ++ 
      (getQuantidadeServicos idOrdemServico (rest itensOrdemServico))
    | otherwise = getQuantidadeServicos (rest idOrdemServico) itensOrdemServico

getValorServicos :: [String] -> [String] -> [String]
getValorServicos _ [] = []
getValorServicos [] _ = []
getValorServicos idOrdemServico itensOrdemServico 
    | (first idOrdemServico) == getN (splitOnComma (first itensOrdemServico)) 0 = 
      [(getN (splitOnComma (first itensOrdemServico)) 6)] ++ 
      (getValorServicos idOrdemServico (rest itensOrdemServico))
    | otherwise = getValorServicos (rest idOrdemServico) itensOrdemServico

getNome :: Int -> Int -> [String] -> String
getNome 0 _ _ = ""
getNome _ _ [] = ""
getNome x y (a:b) | (show x) == getN (splitOnComma a) 0 = getN (splitOnComma a) 1 
                  | otherwise = getNome x y b

totalClientEntre :: Int -> [String] -> Float
totalClientEntre cod lista = do
    let clean_cod = filter (\y -> getN (splitOnComma y) 1 == (show cod)) lista
    let values = map (\y -> getN (splitOnComma y) 5) clean_cod
    let total = totValue values where
        totValue [] = 0
        totValue (a:b) = (read a :: Float) + (totValue b)
    total

qtOrdens :: Int -> [String] -> Int
qtOrdens cod lista = do
    let clean_cod = filter (\y -> getN (splitOnComma y) 1 == (show cod)) lista
    length clean_cod

masc_fem :: String -> String
masc_fem [] = []
masc_fem x | (last x) == 'M' = (init x)++"Masculino"
           | (last x) == 'F' = (init x)++"Feminino"
           | otherwise = x

printCliente [] = return []
printCliente (a:b) = do
    print (masc_fem a)
    printCliente b


printItems [] = return []
printItems (a:b) = do
    print a
    printItems b

printTotal [] = return []
printTotal x = do
    print (value x)
    printTotal [] where
        value :: [String] -> Float
        value [] = 0
        value (a:b) = (read (last (splitOnComma a)) :: Float) + (value b)
    
sortLT (a1, b1, c1) (a2, b2, c2)
    | a1 < a2 = LT
    | a1 > a2 = GT

mySortFunc (a1,a2,a3,a4) (b1,b2,b3,b4) 
    | a3 >= b3 = LT
    | a3 < b3 = GT

mySortFunc2 (a1, a2, a3, a4, a5) (b1, b2, b3, b4, b5) 
    | a5 >= b5 = LT
    | a5 < b5 = GT